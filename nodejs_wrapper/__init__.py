import importlib.resources
import subprocess
import sys

from pathlib import Path


ROOT_PATH = Path(__file__).parent
NODEJS_PATH = ROOT_PATH / "node-v16.13.0-linux-x64" / "bin"


def wrapper(name):
    def func():
        binary_path = NODEJS_PATH / name
        args = [binary_path] + sys.argv[1:]
        subprocess.run(args)

    return func


for binary_name in ("corepack", "node", "npm", "npx"):
    globals()[binary_name] = wrapper(binary_name)
