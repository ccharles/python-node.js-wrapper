from glob import glob

from setuptools import setup


node_distribution = glob("nodejs_wrapper/node-v16.13.0-linux-x64/**/*")


setup(
    name="nodejs-wrapper",
    packages=["nodejs_wrapper"],
    version="16.13.0.0",
    description="A thin, naive, Node.js wrapper in Python",
    url="",
    entry_points={
        "console_scripts": {
            "corepack=nodejs_wrapper:corepack",
            "node=nodejs_wrapper:node",
            "npm=nodejs_wrapper:npm",
            "npx=nodejs_wrapper:npx",
        }
    },
    package_data={
        "nodejs_wrapper": ["node-v16.13.0-linux-x64/bin/*"],
    },
    python_requires=">=3.9",
)
